const myVideo = document.createElement('video');
const videoGrid = document.querySelector('.video-grid');
const midia = {
  video: true,
  audio: true,
};

const addVideoStream = (element, stream) => {
  const video = element;
  video.srcObject = stream;
  videoGrid.append(video);
};

navigator.mediaDevices.getUserMedia(midia)
  .then((stream) => {
    addVideoStream(myVideo, stream);
  });
